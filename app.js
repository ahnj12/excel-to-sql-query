const readXlsxFile = require('read-excel-file/node');
const fs = require('fs');

const tableName = process.argv.slice(2);

readXlsxFile('./az_keyboxes.xlsx').then((rows) => {
    console.log(rows);

    const columnNames = rows[0];

    let query = 'INSERT INTO ' + tableName + ' (';
    let serialNumbers = '(';

    // Insert the column names
    for (let i = 0; i < columnNames.length; i++)
    {
        let columnName = columnNames[i];
        query += columnName;  
        if (i !== (columnNames.length - 1))
        {
            query += ', ';
        }
    }

    query += ') VALUES ('

    for (let i = 1; i < rows.length; i++)
    {
        if (i !== 1)
        {
            query += ', (';
        }

        let row = rows[i];

        for (let j = 0; j < row.length; j++)
        {
            let data = row[j];

            if (j === 0)
            {
                serialNumbers += "'" + data + "'" + ', ';
            }

            if (typeof data === 'string')
            {
                data = data.replace(/"/g, "'");
            }    

            if (j === (row.length - 1))
            {
                query += data + ')';
            }
            else
            {
                query += data + ', ';
            }
        }
    }

    console.log(query);
    console.log();
    // console.log(serialNumbers);
});